CSCI  522 - Android Mobile Device Program
Course for Android Mobile Application Development
Test Application and Assignment over Class as Portfolio.
Assignment 0 : 
	Random Test  demoing the functional and adaptable flow of android application development

Assignment 1 : 
	For this assignment, write an Android application that will function as a Quadratic equation calculator. 
	The standard form of the Quadratic equation is:
	ax2 + bx + c = 0 where a is a non-zero value. b and c may be any value. When the equation is solved, it can have up to two values for x.
	These are the places where the x-axis will be intersected if the equation is graphed. Use the Quadratic formula to determine the x 
	value(s):
	Quadratic Formula = (-b + square root (b2 - 4ac)) / 2a
	To determine how many x values exist, calculate the discriminant:
	Discriminant = b2 - 4ac
	If the discriminant is positive, there are two values for x. One value can be found by solving the Quadratic formula using 
	addition and the other by using subtraction. If the discriminant is 0, there is one value for x with multiplicity of 2, 
	which means that either addition or subtraction can be used when solving the Quadratic formula because both results will be the same.
	If the discriminant is negative, there are no real values for x, only imaginary ones.Design the screen with 3 input text boxes
	(a, b, and c). They should all have labels that are clear enough so that the user knows what value should be put in each box. 
	The boxes should allow both positive and negative numbers to be entered.
	There should be 2 text boxes to display the values for x. Each one should have a clear label. 
	If there is only one value for x, it should be displayed in both boxes. If there is no real value for x, display "Imaginary" 
	(or something similar) in both boxes.	There should be 2 buttons on the screen. One will do the calculation of the x value(s). 
	The other will clear the fields. The calculation button should handle the cases of the user entering a value of 0 and an empty 
	a, b, or c field, by displaying a Toast message and not doing any calculation.development

Assignment 2 :
 Overview

For this assignment, write an Android application that can be used to display information about 5 (at the least) related topics of your choosing. For example: if you're interested in baking, your application could display 5 of your favorite recipes - maybe recipes for chocolate cake, chocolate chip cookies, chocolate pie, brownies, and chocolate ice cream. If you're interested in hockey, your application could display information about 5 teams in the league.

No matter what is selected as the topic, find an image to go along with each of the 5 topics that you select. Make sure the size of the image is appropriate for a phone. A simple photo program such as Paint can be used to resize or crop images if necessary. PNG images are considered the best for Android applications, JPG images are considered acceptable, and GIF images will work but are not the best choice.

The information for each of the topics should have some length to it, meaning that it should be something that is scrollable on a device.

Design and Functionality

The application will consist of two Activities. The main activity will present the 5 topics that the user can choose from. When the user selects a topic the image for that topic should be displayed. If the user wants to see the information for that topic, they will click a button to start the second activity.

Design the main screen of the application to display an appropriate title at the top of the screen. It can be something as simple as a TextView with a string object or it could be an ImageView that holds an image of the title.

Use a Spinner to hold the 5 topics that the user can select from. Also think about adding a label above the spinner to let the user know what they're selecting.

Use an ImageView to display an image of what was selected by the user.

As mentioned above, use a Button to start the second activity.

The second activity is where the information will be displayed.

The design for the second activity can be left pretty basic. At a minimum, it should have a TextView that displays a title, a button that will return the user to the main activity and a scrollable TextView where the information will be displayed.
Assignment 3 :

Assignment 4 :
The application should start with a simple splash screen that displays an image and some type of title for the application.

The main screen of the application should display the current contents of the database. If the database is empty, then nothing will display on the screen or maybe just a simple title like "Your List:" will appear. If the database is holding information, then a simple title like "Your List:" will be followed by a display of all the information in the database. When the information is displayed, it should include a checkbox and description for each item. If the task has been completed, the checkbox should have a check. If the task has not been completed, the checkbox should be empty.

The display can be done with a ListView, by using a ScrollView with a Vertical LinearLayout, or however you decide to implement it.

The functionality for the database should be presented in a menu. At a minimum, the menu should have items to insert into the database, delete from the database, and start over with a new list of items.

When the insert menu option is selected, the user should be presented with a screen that contains, at a minimum, an EditText where the user can enter information (the task to be completed or the grocery item to buy), a Button that will add the item to the database, a Button that will return to the main screen, and a listing of the items that are currently in the database. The listing of items that are currently in the database should be updated with the newest addition to the database every time the Button that adds items to the database is clicked.

When the delete menu option is selected, the user should be presented with a screen with a listing of all the items that are in the database. Each item should be displayed with a radio button along with the description for the item. When the user selects an item, it should be deleted from the database and the listing on the screen should be immediately updated.

When the start over with a new list of items menu option is selected, all the items in the database should be deleted and the listing on the main screen should be cleared of any information.

If you do choose to add more items to the menu, make sure their functionality is thoroughly documented in your code.

The database should be implemented as a separate class. The methods that are part of the class are left up to you, but it would probably be helpful to have methods to:

insert an item, which is passed in as an argument
retrieve all the items in the database
update an item
delete an item by id number
delete all the items in the database
How the information is stored is left up to you but it would probably be useful to create a class to represent a single item that will be added to the database. It should probably hold the unique id number for the database, the description of the task to be completed (or item to be bought), and whether the task has been completed (or item has been bought). It would also probably be helpful to have a complete set of setters and getters for the data members.